import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NombreProductoComponent } from './nombre-producto.component';

describe('NombreProductoComponent', () => {
  let component: NombreProductoComponent;
  let fixture: ComponentFixture<NombreProductoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NombreProductoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NombreProductoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
