import { Component, OnInit, Input, HostBinding} from '@angular/core';
import { nombreProducto} from './../models/nombre-Productos.models';

@Component({
  selector: 'app-nombre-producto',
  templateUrl: './nombre-producto.component.html',
  styleUrls: ['./nombre-producto.component.css']
})
export class NombreProductoComponent implements OnInit {
  @Input() productos:nombreProducto;
  @HostBinding('attr.class') cssClass= 'col-md-4'
  constructor() {

   }

  ngOnInit(): void {
  }

}
