import { Component, OnInit } from '@angular/core';
import { nombreProducto} from './../models/nombre-Productos.models';
@Component({
  selector: 'app-lista-productos',
  templateUrl: './lista-productos.component.html',
  styleUrls: ['./lista-productos.component.css']
})
export class ListaProductosComponent implements OnInit {
productos: nombreProducto[];

constructor() {
this.productos =[];
}

  ngOnInit(): void {
  }
  guardar(nombre: string,url: string): boolean{
    this.productos.push(new nombreProducto(nombre, url));
    return false;
  }

}
